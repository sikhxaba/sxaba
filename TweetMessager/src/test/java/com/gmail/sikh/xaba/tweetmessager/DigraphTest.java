/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gmail.sikh.xaba.tweetmessager;

import com.gmail.sikh.xaba.tweetmessaging.model.TwitterUser;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author sikhumbuzo
 */
@RunWith(MockitoJUnitRunner.class)
public class DigraphTest {
    @InjectMocks
    private Digraph digraph;
    
    @Spy
    private Set<TwitterUser> twitterSet = new HashSet<>();
    @Spy
    private List<String> edges = new ArrayList<>();
    
    @Mock
    private TwitterUser twitterUser;
    
    
    private String name = "Alan";
    
    public DigraphTest() {
        
        MockitoAnnotations.initMocks(this);
        twitterUser.setUserId(1);
        twitterUser.setName("Alan");
        twitterSet.add(twitterUser);
        twitterUser.setUserId(2);
        twitterUser.setName("Ward");
        twitterSet.add(twitterUser);
        twitterUser.setUserId(3);
        twitterUser.setName("Martin");
        twitterSet.add(twitterUser);
        
        edges.add("Alan,Ward");
        edges.add("Martin,Ward");
        
        
        
    }
    
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testCreateGraph() throws Exception{
        when(twitterUser.getName()).thenReturn("Alan");
        Map<TwitterUser, List<TwitterUser>> graphList = digraph.createGraph();
        assertEquals(2, graphList.size());
    }
    
}
