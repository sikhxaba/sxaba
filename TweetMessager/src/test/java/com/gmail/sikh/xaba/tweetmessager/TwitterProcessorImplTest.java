/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gmail.sikh.xaba.tweetmessager;

import com.gmail.sikh.xaba.tweetmessaging.model.Tweet;
import com.gmail.sikh.xaba.tweetmessaging.model.TwitterUser;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author sikhumbuzo
 */
@RunWith(MockitoJUnitRunner.class)
public class TwitterProcessorImplTest {
    @InjectMocks
    private TwitterProcessorImpl processorImpl;
    
    private List<Tweet>tweets = new ArrayList<>();
    
    private Set<TwitterUser> twitterUsers = new HashSet<>();
    
    public TwitterProcessorImplTest() {
        MockitoAnnotations.initMocks(this);
        Tweet tweet = new Tweet();
        tweet.setName("Alan");
        tweet.setTweetMessage("If you have procedure with ten parameters, you probably missed some.");
        tweets.add(tweet);
        
        TwitterUser twitterUser = new TwitterUser();
        twitterUser.setName("Alan");
        twitterUser.setUserId(1);
        twitterUsers.add(twitterUser);
    }
    
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testPublishTweet()throws Exception {
        String expName = "Alan";
        String expMsg = "@Alan:If you have procedure with ten parameters, you probably missed some.";
        processorImpl.publishTweets(tweets, twitterUsers);
        Assert.assertEquals(expName, this);
    }
    
}
