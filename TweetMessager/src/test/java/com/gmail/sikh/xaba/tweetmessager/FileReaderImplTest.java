/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gmail.sikh.xaba.tweetmessager;

import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author sikhumbuzo
 */
@RunWith(MockitoJUnitRunner.class)
public class FileReaderImplTest {
    @InjectMocks
    private FileReaderImpl fileReaderService;
    
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testReadFile()throws Exception {
        System.out.println("Test Read File");
        String expectedResult = "Ward follows Alan";
        String filePath = "C:\\project\\TweetMessager\\src\\main\\resources\\users.txt";
        List<String> fileLines = fileReaderService.readFile(filePath);
        assertEquals(expectedResult, fileLines.get(0));
    }
    
}
