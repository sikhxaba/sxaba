/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gmail.sikh.xaba.tweetmessager;

import com.gmail.sikh.xaba.tweetmessaging.model.Tweet;
import com.gmail.sikh.xaba.tweetmessaging.model.TwitterUser;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author sikhumbuzo
 */
@RunWith(MockitoJUnitRunner.class)
public class DataProcessorImplTest {
    @InjectMocks
    private DataProcessorImpl dataProcessorImpl;
    
    private List<String> dataLines = new ArrayList<>();
    
    private List<String> tweetDataLines = new ArrayList<>();
    
    public DataProcessorImplTest() {
    }
    
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        
        
    }
    
    @After
    public void tearDown() {
        dataLines = new ArrayList<>();
    }

    @Test
    @Ignore
    public void testCreateTwitterUsers()throws Exception {
        dataLines.add("Ward follows Alan,Martin");
        Set<TwitterUser> users = new HashSet<>();
        TwitterUser twitterUser = new TwitterUser();
        twitterUser.setName("Alan");
        twitterUser.setUserId(1);
        users.add(twitterUser);
        twitterUser = new TwitterUser();
        twitterUser.setName("Ward");
        twitterUser.setUserId(2);
        users.add(twitterUser);
        twitterUser = new TwitterUser();
        twitterUser.setName("Martin");
        twitterUser.setUserId(3);
        users.add(twitterUser);
        Set<TwitterUser> twitterUsers = dataProcessorImpl.createTwitterUsers(dataLines);
        assertEquals(users.size(), twitterUsers.size());
    }
    
    @Test(expected = IllegalArgumentException.class)
    @Ignore
    public void testCreateTwitterUserWithoutFollower()throws Exception {
        dataLines.add("Ward follows");
        when(dataProcessorImpl.createTwitterUsers(dataLines)).thenThrow(new IllegalArgumentException("Invalid format"));
    }
    
    @Test
    @Ignore
    public void testUserWithOneFollower()throws Exception{
        dataLines.add("Ward follows Martin");
        Set<TwitterUser> users = new HashSet<>();
        TwitterUser twitterUser = new TwitterUser();
        twitterUser.setName("Martin");
        twitterUser.setUserId(1);
        users.add(twitterUser);
        twitterUser = new TwitterUser();
        twitterUser.setName("Ward");
        twitterUser.setUserId(2);
        users.add(twitterUser);
        Set<TwitterUser> twitterUsers = dataProcessorImpl.createTwitterUsers(dataLines);
        assertEquals(users.size(), twitterUsers.size());
    }
    
    @Test
    @Ignore
    public void testCreateTweets()throws Exception{
        tweetDataLines.add("Alan>If you have a procedure with 10 parameters, you probably missed some");
        tweetDataLines.add("Alan>hehehe");
        List<Tweet> xpTweet = new ArrayList<>();
        Tweet tweet = new Tweet();
        tweet.setName("Alan");
        tweet.setTweetMessage("If you have a procedure with 10 parameters, you probably missed some");
        xpTweet.add(tweet);
        tweet = new Tweet();
        tweet.setName("Alan");
        tweet.setTweetMessage("hehehe");
        xpTweet.add(tweet);
        List<Tweet> tweets = dataProcessorImpl.createTweets(tweetDataLines);
        assertEquals(xpTweet.size(), tweets.size());
    }
    @Ignore
    @Test(expected = IllegalArgumentException.class)
    public void testCreateTwitterMsgWrongFormat()throws Exception {
        dataLines.add("Ward follows");
        when(dataProcessorImpl.createTweets(dataLines)).thenThrow(new IllegalArgumentException("Invalid format"));
    }
    @Test
    public void testCreateSingleEdge()throws Exception{
        dataLines.add("Ward follows Martin");
        String edge = "Martin,Ward";
        List<String> edges = dataProcessorImpl.createEdges(dataLines);
        assertEquals(edge, edges.get(0));
    }
    
    @Test
    @Ignore
    public void testCreateMultipleEdges()throws Exception{
        dataLines.add("Ward follows Martin,Alan");
        List<String> edges = dataProcessorImpl.createEdges(dataLines);
        assertEquals(2, edges.size());
    }
}
