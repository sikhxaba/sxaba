/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gmail.sikh.xaba.tweetmessager;

import com.gmail.sikh.xaba.tweetmessaging.api.DataProcessorService;
import com.gmail.sikh.xaba.tweetmessaging.model.Tweet;
import com.gmail.sikh.xaba.tweetmessaging.model.TwitterUser;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author sikhumbuzo
 */
public class DataProcessorImpl implements DataProcessorService {

    @Override
    public Set<TwitterUser> createTwitterUsers(List<String> dataLines) {
        Set<TwitterUser> users = new HashSet<>();
        String separator = "follows";
        String commaSeparator = ",";
        for (String dataLine : dataLines) {
            if (!(dataLine.contains(separator))) {
                throw new IllegalArgumentException(dataLine + " has an invalid format");
            } else {
                String[] values = dataLine.split(separator);
                addTweetUser(users, values[0].trim());
                if (values.length > 1) {
                    if (values[1].contains(commaSeparator)) {
                        String[] secondSplit = values[1].split(commaSeparator);
                        for (String followedNames : secondSplit) {
                            addTweetUser(users, followedNames.trim());
                        }

                    } else {
                        if (!(values[1].contains(commaSeparator))) {
                            addTweetUser(users, values[1].trim());
                        }

                    }
                } else {
                    throw new IllegalArgumentException(" data has invalid format");
                }
            }

        }
        return users;
    }

    @Override
    public List<Tweet> createTweets(List<String> dataLines) {
        String separator = ">";
        List<Tweet> tweets = new ArrayList<Tweet>();
        for (String dataLine : dataLines) {
            if (!(dataLine.contains(separator))) {
                throw new IllegalArgumentException(dataLine + " has an illegal format");
            }
            String[] usrTweetData = dataLine.split(separator);
            Tweet tweet = new Tweet();
            tweet.setName(usrTweetData[0]);
            tweet.setTweetMessage(usrTweetData[1]);
            tweets.add(tweet);
        }
        return tweets;
    }

    private void addTweetUser(Set<TwitterUser> users, String name) {
        TwitterUser twtUser = new TwitterUser();
        if (users.size() == 0) {
            twtUser.setName(name.trim());
            twtUser.setUserId(users.size());
            users.add(twtUser);
        } else if (users.size() > 0) {
            if(checkVertexExistence(users, name)){
                twtUser.setName(name.trim());
                twtUser.setUserId(users.size());
                users.add(twtUser);
            }
                
        }

    }

    private boolean checkVertexExistence(Set<TwitterUser> users, String name) {
        boolean check = true;
        for (TwitterUser user : users) {
            final String usrName = user.getName().trim();
            if(usrName.equals(name)){
              check = false;
            }
        }
        return check;
    }

    @Override
    public List<String> createEdges(List<String> dataLines) {
        String separator = "follows";
        String commaSeparator = ",";
        List<String> edges = new ArrayList<>();
        String edge = null;
        for (String usrData : dataLines) {
            if (!(usrData.contains(separator))) {
                throw new IllegalArgumentException(usrData + " has an invalid format");
            } else {
                String[] names = usrData.split(separator);
                if (!(names[1].contains(commaSeparator)) && (null != names[1])) {
                    edge = names[1] + "," + names[0];
                    edges.add(edge);
                } else {
                    if (names[1].contains(commaSeparator)) {
                        String[] splitNames = names[1].split(commaSeparator);
                        for (String splitname : splitNames) {
                            edge = splitname + "," + names[0];
                            edges.add(edge);
                        }
                    }
                }

            }
        }
        return edges;

    }
}
