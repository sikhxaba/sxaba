/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gmail.sikh.xaba.tweetmessager;

import java.util.Set;
import com.gmail.sikh.xaba.tweetmessaging.model.TwitterUser;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author sikhumbuzo
 */
public class Digraph implements Serializable {

    private static final long serialVersionUID = 3166542766899496077L;

    private Map<TwitterUser, List<TwitterUser>> twitterAdjacencyMap = new HashMap<TwitterUser, List<TwitterUser>>();

    private Set<TwitterUser> users;

    private List<String> edges;
    
    private static final  int SRC_NAME_NODE = 0;
    
    private static final int DEST_NAME_NODE = 1;

    public Digraph(Set<TwitterUser> users, List<String> edges) {
        this.users = users;
        this.edges = edges;
        for (TwitterUser user : users) {
            twitterAdjacencyMap.put(user, new LinkedList<>());
        }
    }

    public Map<TwitterUser, List<TwitterUser>> getTwitterAdjacencyMap() {
        return twitterAdjacencyMap;
    }

    public Map<TwitterUser, List<TwitterUser>> createGraph() {
        for (TwitterUser user : users) {
            for (String edge : edges) {
                String[] splitEdge = edge.split(",");
                final String usrName = user.getName().trim();
                if(usrName.equalsIgnoreCase(splitEdge[SRC_NAME_NODE].trim())){
                    //Search destination vertex
                    TwitterUser destinationUsr = findDestVertex(splitEdge[DEST_NAME_NODE]);
                    if(null != destinationUsr){
                        twitterAdjacencyMap.get(user).add(destinationUsr);
                    }
                    
                }
            }
        }
        return twitterAdjacencyMap;
    }

    private TwitterUser findDestVertex(String val) {
        for (TwitterUser user : users) {
            if(user.getName().trim().equalsIgnoreCase(val.trim())){
                return user;
            }
        }
        return null;
        
    }


}
