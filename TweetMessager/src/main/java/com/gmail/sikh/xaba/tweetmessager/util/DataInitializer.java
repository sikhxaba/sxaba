/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gmail.sikh.xaba.tweetmessager.util;

import com.gmail.sikh.xaba.tweetmessager.DataProcessorImpl;
import com.gmail.sikh.xaba.tweetmessager.FileReaderImpl;
import com.gmail.sikh.xaba.tweetmessaging.api.DataProcessorService;
import com.gmail.sikh.xaba.tweetmessaging.api.FileReaderService;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sikhumbuzo
 */
public class DataInitializer {
    
    private static DataInitializer instance = null;
    
    private DataProcessorService dataProcessorService = new DataProcessorImpl();
    
    private FileReaderService fileReaderService = new FileReaderImpl();

    private DataInitializer() {
        String filePath = null;
        try {
            fileReaderService.readFile(filePath);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DataInitializer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static DataInitializer getInstance(){
        
        if(instance == null){
            instance = new DataInitializer();
        }
        return instance;
    }
    
}
