/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gmail.sikh.xaba.tweetmessager;

import com.gmail.sikh.xaba.tweetmessaging.api.FileReaderService;
import com.gmail.sikh.xaba.tweetmessaging.model.Tweet;
import com.gmail.sikh.xaba.tweetmessaging.model.TwitterUser;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sikhumbuzo
 */
public class TwitterMain {

    private FileReaderService fileReaderService = new FileReaderImpl();
    DataInitializer dataInitializer = new DataInitializer();

    private Digraph digraph;

    private Map<TwitterUser, List<String>> map = new HashMap<>();

    public void initialize(String filePath, String tweetFilePath) {
        try {
            List<String> dataLines = fileReaderService.readFile(filePath);
            dataInitializer.createUsrData(dataLines);
            dataInitializer.createTwtEgdes(dataLines);
            List<String> tweetMsgs = fileReaderService.readFile(tweetFilePath);
            dataInitializer.createTweetsData(tweetMsgs);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TwitterMain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void createGraph() {
        digraph = new Digraph(dataInitializer.getUsers(), dataInitializer.getEdges());
        Map<TwitterUser, List<TwitterUser>> graph = digraph.createGraph();

    }

    public void processTweets() {
        Set<TwitterUser> users = dataInitializer.getUsers();
        createListForEachUsr(users, map);
        Map<TwitterUser, List<TwitterUser>> twitterAdjacencyMap = digraph.getTwitterAdjacencyMap();
        for (TwitterUser user : users) {
            List<String> messages = findUsrTweetMsgs(user.getName());
            List<String> list = map.get(user);
            list.addAll(messages);
            map.put(user, list);
            if (messages.size() > 0) {
                List<TwitterUser> tweetUsrs = twitterAdjacencyMap.get(user);
                if (tweetUsrs.size() > 0) {
                    for (TwitterUser tweetUsr : tweetUsrs) {
                        List<String> origList = map.get(tweetUsr);
                        origList.addAll(messages);
                        map.put(tweetUsr, origList);
                    }
                }
            }
        }
    }

    private List<String> findUsrTweetMsgs(String name) {
        List<String> usrMsgs = new ArrayList<>();
        for (Tweet tweet : dataInitializer.getTweets()) {
            if (name.equalsIgnoreCase(tweet.getName())) {
                usrMsgs.add(tweet.toString());
            }
        }
        return usrMsgs;
    }

    public void publishTweets() {
        for (Map.Entry<TwitterUser, List<String>> entry : map.entrySet()) {
            TwitterUser key = entry.getKey();
            List<String> value = entry.getValue();
            System.err.println(key.toString());
            for (String message : value) {
                System.out.println(message.toString());
            }

        }
    }

    public static void main(String[] args) {

        TwitterMain twitterProcessorImpl = new TwitterMain();
        twitterProcessorImpl.initialize(args[0], args[1]);
        twitterProcessorImpl.createGraph();
        twitterProcessorImpl.processTweets();
        twitterProcessorImpl.publishTweets();

    }

    private void createListForEachUsr(Set<TwitterUser> users, Map<TwitterUser, List<String>> map) {
        List<String> msgs = null;
        for (TwitterUser user : users) {
            msgs = new ArrayList<>();
            map.put(user, msgs);
        }
    }

}
