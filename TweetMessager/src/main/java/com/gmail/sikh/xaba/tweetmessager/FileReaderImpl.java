/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gmail.sikh.xaba.tweetmessager;

import com.gmail.sikh.xaba.tweetmessaging.api.FileReaderService;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author sikhumbuzo
 */
public class FileReaderImpl implements FileReaderService{

    @Override
    public List<String> readFile(String filePath) {
        Path path = Paths.get(filePath);
        List<String> dataLines = null;
        try{
           if(path != null) {
               Stream<String> lines = Files.lines(path);
               dataLines = lines.collect(Collectors.toList());
           }
        }catch(FileNotFoundException e){
            Logger.getLogger(FileReaderImpl.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException ex) {
            Logger.getLogger(FileReaderImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dataLines;
        
    }

    
}
