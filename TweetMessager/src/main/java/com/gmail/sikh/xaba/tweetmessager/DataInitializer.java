/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gmail.sikh.xaba.tweetmessager;

import com.gmail.sikh.xaba.tweetmessaging.api.DataProcessorService;
import com.gmail.sikh.xaba.tweetmessaging.model.Tweet;
import com.gmail.sikh.xaba.tweetmessaging.model.TwitterUser;
import java.util.List;
import java.util.Set;

/**
 *
 * @author sikhumbuzo
 */
public class DataInitializer {
    
    private Set<TwitterUser> users;
    
    private List<String> edges;
    
    private List<Tweet>tweets;
    
    private DataProcessorService dataProcessorService = new DataProcessorImpl();
    
    
    public void createUsrData(List<String> dataLines){
        users = dataProcessorService.createTwitterUsers(dataLines);
    }
    
    public void createTwtEgdes(List<String>dataLines){
        edges = dataProcessorService.createEdges(dataLines);        
    }
    
    public void createTweetsData(List<String> dataLines){
        tweets = dataProcessorService.createTweets(dataLines);
    }

    public Set<TwitterUser> getUsers() {
        return users;
    }

    public List<String> getEdges() {
        return edges;
    }

    public List<Tweet> getTweets() {
        return tweets;
    }
    
}
