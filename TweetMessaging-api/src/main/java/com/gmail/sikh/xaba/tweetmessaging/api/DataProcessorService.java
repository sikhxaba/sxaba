/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gmail.sikh.xaba.tweetmessaging.api;

import com.gmail.sikh.xaba.tweetmessaging.model.Tweet;
import com.gmail.sikh.xaba.tweetmessaging.model.TwitterUser;
import java.util.List;
import java.util.Set;

/**
 *
 * @author sikhumbuzo
 */
public interface DataProcessorService {
    
    Set<TwitterUser> createTwitterUsers(List<String> dataLines);
    
    List<Tweet> createTweets(List<String> dataLines);
    
    List<String> createEdges(List<String> dataLines);
}
