/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gmail.sikh.xaba.tweetmessaging.api;

import java.io.FileNotFoundException;
import java.util.List;

/**
 *
 * @author sikhumbuzo
 */
public interface FileReaderService {
    
    List<String>readFile(String filePath)throws FileNotFoundException;
    
}
