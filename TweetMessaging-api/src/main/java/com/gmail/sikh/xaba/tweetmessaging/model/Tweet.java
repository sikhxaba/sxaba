/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gmail.sikh.xaba.tweetmessaging.model;

import java.io.Serializable;

/**
 *
 * @author sikhumbuzo
 */
public class Tweet implements Serializable{
    
    private static final long serialVersionUID = -6166003816090879630L;
    
    private String name;
    
    private String tweetMessage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTweetMessage() {
        return tweetMessage;
    }

    public void setTweetMessage(String tweetMessage) {
        this.tweetMessage = tweetMessage;
    }

    @Override
    public String toString() {
        return "@" + name + ":" + tweetMessage;
    }
    
}
