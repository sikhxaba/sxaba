/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gmail.sikh.xaba.tweetmessaging.model;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author sikhumbuzo
 */
public class TwitterUser implements Serializable {
    
    private static final long serialVersionUID = -3719510226385612424L;
    
    private int userId;
    
    private String name;

    public TwitterUser() {
    }

    public TwitterUser(String name) {
        this.name = name;
    }
    

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TwitterUser other = (TwitterUser) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    

    @Override
    public String toString() {
        return  name;
    }
    
}
